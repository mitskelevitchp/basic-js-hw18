// Практичне завдання
/*Реализовать функцию полного клонирования объекта.
1. Написать функцию для рекурсивного полного клонирования объекта (без единой передачи по ссылке, внутренняя вложенность свойств объекта может быть достаточно большой).
2. Функция должна успешно копировать свойства в виде объектов и массивов на любом уровне вложенности.
3. В коде нельзя использовать встроенные механизмы клонирования, такие как функция Object.assign() или спред-оператор.*/

// Версия черновая

// const objectOne = {
//   first: 1,
//   second: "two",
//   3: [1, 2, 3, 4, "string"],
//   objectInsideOne: {
//     propertyOne: "1",
//     propertyTwo: 2,
//     objectInsideTwo: {
//       name: "Stepan",
//       lastname: "Bandera",
//     },
//   },
//   func: function () {
//     return (
//       this.objectInsideOne.objectInsideTwo.name +
//       " " +
//       this.objectInsideOne.objectInsideTwo.lastname +
//       " is number " +
//       '"' +
//       this["3"][0] +
//       '"!'
//     );
//   },
// };

// let cloneSomeObject = (obj) => {
//   const clone = {};
//   for (let key in obj) {
//     if (typeof obj[key] === "object") {
//       clone[key] = cloneSomeObject(obj[key]);
//     } else clone[key] = obj[key];
//   }
//   return clone;
// };
// const objectTwo = cloneSomeObject(objectOne);

// console.log(objectOne);
// console.log(objectTwo);

// Версия основная
const objectOne = {
  first: 1,
  second: "two",
  3: [1, 2, 3, 4, "string"],
  objectInsideOne: {
    propertyOne: "1",
    propertyTwo: 2,
    objectInsideTwo: {
      name: "Stepan",
      lastname: "Bandera",
    },
  },
  func: function () {
    return (
      this.objectInsideOne.objectInsideTwo.name +
      " " +
      this.objectInsideOne.objectInsideTwo.lastname +
      " is number " +
      this["3"][0] +
      "!"
    );
  },
};

let cloneSomeObject = (obj) => {
  const clone = {};
  for (let key in obj) {
    if (Array.isArray(obj[key])) {
      clone[key] = obj[key].slice(0);
    } else if (typeof obj[key] === "object") {
      clone[key] = cloneSomeObject(obj[key]);
    } else clone[key] = obj[key];
  }
  return clone;
};
const objectTwo = cloneSomeObject(objectOne);

console.log(objectOne);
console.log(objectTwo);
